FROM openjdk:11.0-jre-slim

WORKDIR /usr/local/runme
COPY target/*.war webapp.war

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "webapp.war"]