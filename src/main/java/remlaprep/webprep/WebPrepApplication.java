package remlaprep.webprep;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebPrepApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebPrepApplication.class, args);
	}

}
