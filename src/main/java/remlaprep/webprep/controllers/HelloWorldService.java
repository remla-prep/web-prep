package remlaprep.webprep.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import remla.RemlaUtil;

@Controller
public class HelloWorldService {

	private int accesses = 0;

	private int numPredictions = 0;
	private int numAgreed = 0;

	public void addAccess() {
		accesses++;
	}

	@GetMapping("/")
	@ResponseBody
	public String index() {
		addAccess();
		String m = "Hello World from %s! once more!! (utils version: %s)";
		return String.format(m, RemlaUtil.getHostName(), RemlaUtil.getVersion());
	}

	@GetMapping(value = "/metrics", produces = "text/plain")
	@ResponseBody
	public String metrics() {
		StringBuilder sb = new StringBuilder();

		sb.append("# HELP my_random A random number in the interval [0,1[\n");
		sb.append("# TYPE my_random gauge\n");
		sb.append("my_random ").append(Math.random()).append("\n\n");

		sb.append("# HELP accesses_total Total number of HTTP requests\n");
		sb.append("# TYPE accesses_total counter\n");
		sb.append("accesses_total ").append(accesses).append("\n\n");

		double accuracy = 0.0;
		if (numPredictions > 0) {
			accuracy = numAgreed / (double) numPredictions;
		}
		sb.append("# HELP accuracy Accuracy of spam prediction\n");
		sb.append("# TYPE accuracy gauge\n");
		sb.append("accuracy ").append(accuracy).append("\n\n");

		return sb.toString();
	}

	public void addPrediction(boolean isAgreed) {
		numPredictions++;
		if (isAgreed) {
			numAgreed++;
		}
	}
}