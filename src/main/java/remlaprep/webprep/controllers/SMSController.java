package remlaprep.webprep.controllers;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import remla.RemlaUtil;
import remlaprep.webprep.data.Sms;
import remlaprep.webprep.data.SmsClassification;

@Controller
@RequestMapping(path = "/sms")
public class SMSController {

	private final RestTemplateBuilder rest;
	private final String modelHost;
	private final HelloWorldService hws;

	public SMSController(RestTemplateBuilder rest, Environment env, HelloWorldService hws) {
		this.hws = hws;
		modelHost = env.getProperty("MODEL_HOST");
		System.out.printf("Using '%s' as model host.\n", modelHost);
		this.rest = rest;
	}

	@GetMapping("/")
	public String index(Model model) {
		hws.addAccess();
		model.addAttribute("hostname", RemlaUtil.getHostName());
		return "sms/index";
	}

	@PostMapping("/")
	@ResponseBody
	public SmsClassification predict(@RequestBody Sms q) {

		hws.addAccess();
		String url = modelHost + "/predict";
		ResponseEntity<SmsClassification> res = rest.build().postForEntity(url, q, SmsClassification.class);

		String pred = res.getBody().result.toLowerCase().trim();
		String guess = q.guess.toLowerCase().trim();
		hws.addPrediction(pred.equals(guess));
		
		
		return res.getBody();
	}
}